const express = require('express')
const mysql = require('mysql')
var bodyParser = require('body-parser')

const app = express()
const port = 3000

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "password",
    database: "movies",
    debug: false
});

app.use(bodyParser.json())

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

app.get('/users', (req, res) => {        

    let sql = "";

    if (req.query.limit || req.query.offset) {
        
        sql = 'SELECT * FROM users ' + (req.query.limit ? "LIMIT " + req.query.limit + '' : "") + (req.query.offset ? "OFFSET " + req.query.offset : "")            
        
    } else {
        sql = `SELECT * FROM users`
    } 
    
    con.query(sql, function (err, result) {
        if (err) res.json(err);            
        res.json(result)        
    });
})

app.post('/users', (req, res) => {

    let date = new Date().toLocaleDateString()
    let time = new Date().toLocaleTimeString()
    let datetime = date + " " + time
    
    let sql = `INSERT INTO users (pseudo, email, firstname, lastname, createdAt) VALUES ("${req.body.pseudo}", "${req.body.email}", "${req.body.firstname}", "${req.body.lastname}", "${datetime}")`
    
    con.query(sql, function (err, result) {
        if (err) res.json(err);            
        res.json(result)
    });
})

app.get('/users/:id', (req, res) => {

    let sql = `SELECT * FROM users WHERE id = ${req.params.id}`
    
    con.query(sql, function (err, result) {
        if (err) res.json(err);        
        res.json(result)
    });
})

app.delete('/users/:id', (req, res) => {

    let sql = `DELETE FROM users WHERE id = ${req.params.id}`

    con.query(sql, function (err, result) {
        if (err) res.json(err);        
        res.json(result)
    });
})

app.put('/users/:id', (req, res) => {

    let date = new Date().toLocaleDateString()
    let time = new Date().toLocaleTimeString()
    let datetime = date + " " + time

    let sql = `UPDATE users SET pseudo = "${req.body.pseudo}", email = "${req.body.email}", firstname = "${req.body.firstname}", lastname = "${req.body.lastname}", updatedAt = "${datetime}"`

    con.query(sql, function (err, result) {
        if (err) res.json(err);        
        res.json(result)
    });
})